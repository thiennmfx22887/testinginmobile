Testing In Mobile

this is project testing mobile app on android emulator(Drink order applycation)

the main task are:

- Read and analysis system requiments
- Write functional test case and interface on many device size
- Record all the error found in file Defect log
- Collet and evaluate results
- Propose system improvements

Deploy on local, clone project:

- With https
  git clone https://gitlab.com/thiennmfx22887/testinginmobile.git
- With ssh
  git clone git@gitlab.com:thiennmfx22887/testinginmobile.git

Must skills knowledge

- Requirement analysis
- Write test case, logbug
- Know how to the install emulator android on personal computer
